const Discord = require('discord.js');
const colors = require('colors');
const fs = require('fs');

const mysql = require('mysql');
const axios = require('axios');

const config = require('./config/config.json');
const globalVar = require('./global.js');
const lib = require('./lib.js');

const client = new Discord.Client();
 
client.on(`ready`, () => {
	lib.elementsConsole(colors, `INFO`, `Logged in as ${client.user.tag}!`);
});

client.on(`message`, message => {
	if(!message.content.startsWith(config.prefix) || message.author.bot) return;

	let command = message.content.substr(1).split(' ')[0];
	let args = message.content.split(' ').slice(1);

	lib.performCommand(client, message, fs, axios, mysql, colors, command, args);
});

client.login(config.token);