module.exports = {
	// Commands functions
	performCommand: (client, message, fs, axios, mysql, colors, command, args) => {
		if(fs.existsSync(`./commands/${command}.js`)) {
			let execute = require(`./commands/${command}.js`);

			execute[`${command}`](client, message, module.exports, axios, mysql, colors, args);

			module.exports.elementsConsole(colors, `LOG`, `performed !${command}`, message.author.username);
		}
		else {
			module.exports.elementsStatut(message, `Erreur`, embedRed, ``, `Commande non trouvée`);
		}
	},

	// Statut functions
	elementsStatut: (message, type, embedColor, title, text) => {
		message.channel.send({ embed : {
			color: embedColor,
			title: `${type} : ${title}`,
			description: text
		}});
	},

	// Utils functions
	elementsConsole: (colors, type, text, person=``) => {
		console.info(colors.cyan(`[${new Date()}]`)+colors.green(`[${type}]`)+` `+colors.red(person)+` ${text}`);
	}
}