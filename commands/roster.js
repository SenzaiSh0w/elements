module.exports = {
	roster: (client, message, lib, axios, mysql, colors, args) => {
		if(args[0]) {
			message.guild.members.fetch().then(() => {
				let roles = { "tanks": "Tank", "heals": "Heal", "rdps": "DPS Range", "mdps": "DPS Melee" };
				let players_count = { "tanks": 0, "heals": 0, "rdps": 0, "mdps": 0 }
				let players_string = { "tanks": "\u200B", "heals": "\u200B", "rdps": "\u200B", "mdps": "\u200B" };
				let total_players = 0;

				for (var key in roles) {
					let role_name = roles[key];

					if(message.guild.roles.cache.find(role => role.name.toLowerCase() === `Raider ${args[0]} - ${role_name}`.toLowerCase())) {
						let players = message.guild.roles.cache.find(role => role.name.toLowerCase() === `Raider ${args[0]} - ${role_name}`.toLowerCase()).members.map(m => [m.user.username, m.nickname, m.roles.cache.find(r => r.color === 15790320).name, m.roles.cache.some(r => r.name === "Absent")]);

						roles[key] = players;

						players.forEach(player => {
							if(!player[3]) {
								if(player[1] !== null)
									player[0] = player[1];

								players_string[key] += `\n${client.emojis.cache.find(emoji => emoji.name === player[2])} ${player[0]}`;
								players_count[key]++;
								total_players++;
							}
						});
					}
				}

				// console.log(roles);

				message.channel.send({ embed : {
					color: embedElements,
					title: `Roster Raider ${args[0].toUpperCase()} - ${total_players} personne(s)`,
					description: `Liste complète des joueurs "Raider ${args[0].toUpperCase()}" pour le palier actuel : Château de Nathria`,
					thumbnail: {
						url: `http://vps-2e004d5e.vps.ovh.net/static/wow/thumbnails/nathria.jpg`,
					},
					fields : [
						{
							name: `${client.emojis.cache.find(emoji => emoji.name === "tank")} Tank (${players_count['tanks']})`,
							value: players_string['tanks'],
							inline: true
						},
						{
							name: `${client.emojis.cache.find(emoji => emoji.name === "heal")} Heal (${players_count['heals']})`,
							value: players_string['heals'],
							inline: true
						},
						{
							name: `${client.emojis.cache.find(emoji => emoji.name === "dps")} DPS Distant (${players_count['rdps']})`,
							value: players_string['rdps'],
							inline: true
						},
						{
							name: `${client.emojis.cache.find(emoji => emoji.name === "dps")} DPS Melee (${players_count['mdps']})`,
							value: players_string['mdps'],
							inline: true
						}
					]
				}});
			});
		}
	}
}