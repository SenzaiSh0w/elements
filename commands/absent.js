module.exports = {
	absent: (client, message, lib, axios, mysql, colors, args) => {
		message.guild.members.fetch().then(() => {
			let difficulties = ["nm", "hm", "mm"];
			let roles = { "tanks": "Tank", "heals": "Heal", "rdps": "DPS Range", "mdps": "DPS Melee" };

			let players_string = "";
			let total_players = 0;

			if(message.guild.roles.cache.find(role => role.name.toLowerCase() === `Absent`.toLowerCase())) {
				message.guild.roles.cache.find(role => role.name.toLowerCase() === `Absent`.toLowerCase()).members.map(m => [m, m.user.username, m.nickname]).forEach(player => {
					let emoji = "";

					if(player[0].roles.cache.some(r => r.color === 15790320)) {
						emoji = `${client.emojis.cache.find(emoji => emoji.name === player[0].roles.cache.find(r => r.color === 15790320).name)} `;
					}

					if(player[2] !== null)
						player[1] = player[2];

					players_string += `\n${emoji}${player[1]}`;
					total_players++;
				});
			}

			// console.log(roles);

			if(players_string != '') {
				message.channel.send({ embed : {
					color: embedElements,
					title: `Absents - ${total_players} personne(s)`,
					description: `Liste complète des joueurs absents`,
					thumbnail: {
						url: `http://vps-2e004d5e.vps.ovh.net/static/wow/thumbnails/nathria.jpg`,
					},
					fields : [
						{
							name: `Joueurs :`,
							value: players_string,
							inline: true
						}
					]
				}});
			}
			else {
				lib.elementsStatut(message, `Erreur`, embedRed, ``, `Aucun joueurs trouvés`);
			}
		});
	}
}